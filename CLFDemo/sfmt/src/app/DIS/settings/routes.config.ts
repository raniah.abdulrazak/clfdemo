import { Routes } from '@angular/router';
import { AppTemplateRoutes } from '@dis/settings/routes/routes.template.config';
import { RoleTypes } from '@dis/auth/roles.enum';
import { DashboardOneComponent } from '@dis/views/dashboard-one/dashboard-one.component';
import { DashboardTwoComponent } from '@dis/views/dashboard-two/dashboard-two.component';
import { DashboardThreeComponent } from '@dis/views/dashboard-three/dashboard-three.component';


// Import your app views below and add the array below
// For reference, see routes.template.config.js

export const AppRoutes: Routes = [
  // Define a default redirect
  { path: '', pathMatch: 'full', redirectTo: '/dashboard-one' },
  {
    path: 'dashboard-one',
    component: DashboardOneComponent,
    // canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [

      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'dashboard-two',
    component: DashboardTwoComponent,
    // canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [

      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'dashboard-three',
    component: DashboardThreeComponent,
    // canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [

      ] // List out all roles that are acceptable
    }
  },
  { path: '**', redirectTo: '' },
  ...AppTemplateRoutes
];
