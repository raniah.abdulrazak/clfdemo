import {Station} from '@dis/model/station'

export class OperatorExec{
    id: number
    operatorNo: string
    operatorName: string
    checkinTime: Date
    checkoutTime: Date
    checkoutStaffNo: string
    checkoutStaffName : string
    station : Station
}