import {Product} from '@dis/model/product'
import {Station} from '@dis/model/station'

export class ProductionLineSetting{
    productId:number
    shiftId:number
    actualTarget:number
    batchNo: string
    entryDateTime: Date
    createdBy: string
    modifiedBy: string
    productionLineName: string
    statusId: number
    station: Station
    product: Product
}