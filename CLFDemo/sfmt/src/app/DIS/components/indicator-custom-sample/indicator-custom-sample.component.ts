import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { ApiService } from '@dis/services/clfdemo/api.service';

@Component({
  selector: 'app-indicator-custom-sample',
  templateUrl: './indicator-custom-sample.component.html',
  styleUrls: ['./indicator-custom-sample.component.scss']
})


export class IndicatorCustomSampleComponent implements OnInit {
  @Input() name: string;
  @Input() index: string;
  @Input() group: string;
  @Input() callout: string;
  @Input() details: string[];
  @Input() size: string;
  @Input() status: 'success'|'warning'|'error';
  @Input() nextRouteLink: string;

  textIndex
  text = []
  //id


  constructor(private router: Router, private apiService: ApiService) {
  }
  Navigateto= function () {
    this.router.navigateByUrl(this.nextRouteLink);
};
  ngOnInit(): void {
    let self = this;
    console.log("HELLLOOOOOOOOOOO" + this.name);
    this.textIndex = parseInt(this.index);
   
    this.apiService.getTextData().subscribe(
      (res: any) => {
        if (res != undefined && res!= null) {
          this.apiService.textData = res;
          this.text = this.apiService.textData;
          
          console.log("TEXTTTTTTT" + this.text);

        }});

        setInterval(function() {
         
      
          self.apiService.getTextData().subscribe(
            (res: any) => {
              if (res != undefined && res!= null) {
               // this.id = res.id;
                self.apiService.textData = res;
                self.text = self.apiService.textData;
                //alert("TEXTTTTTTT" + res);

                (document.getElementById("img_0") as HTMLImageElement).src= "assets/img/my0.jpeg?update="+ +new Date();
                (document.getElementById("img_1") as HTMLImageElement).src= "assets/img/my1.jpeg?update="+ +new Date();
                (document.getElementById("img_2") as HTMLImageElement).src= "assets/img/my2.jpeg?update="+ +new Date();
                (document.getElementById("img_3") as HTMLImageElement).src= "assets/img/my3.jpeg?update="+ +new Date();  

                document.getElementById("text_0").innerHTML= self.text[0];
                document.getElementById("text_1").innerHTML= self.text[1];
                document.getElementById("text_2").innerHTML= self.text[2];
                document.getElementById("text_3").innerHTML= self.text[3];
      
              }});
          

          }, 5000);
  }


  



}
