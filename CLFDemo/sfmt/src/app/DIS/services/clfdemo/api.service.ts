import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  //readonly defaultUrl = 'http://localhost:3080/api';
  textData = ["","","",""]
  
  getTextData():Observable<any>{
    return this.http.get<any>( 'api/textdata');

  }

  
  


}
