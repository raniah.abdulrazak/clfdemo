import { Component } from '@angular/core';
import { ApiService } from '@dis/services/clfdemo/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: []
})
export class AppComponent {}
