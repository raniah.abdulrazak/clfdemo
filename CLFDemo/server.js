const express = require('express');
var cors = require('cors');
const app = express(),
      bodyParser = require("body-parser");
      port = 3080;

 global.textArr = ["Camera 1","Camera 2","Camera 3","Camera 4"];
app.use(cors());

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(express.json());
app.use(express.static(process.cwd()+"/sfmt/dist/dis-template/"));


app.get('/api/textdata', (req, res) => {
  res.json(textArr);
});



app.post('/api/image', (req, res) => {
    var img = req.body.image;
    var text = req.body.text;
    var id = req.body.id;
    textArr[id] = text;
    console.log("text of " + id + " is ---" + textArr[id])
    console.log(textArr);

    var fs = require('fs');

    var data = img.replace(/^data:image\/\w+;base64,/, "");
    var buf = Buffer.from(data, 'base64');
    var fname = 'sfmt/dist/dis-template/assets/img/my'+id+'.jpeg';
    var result ="Success";
   // var fname = 'sfmt/src/assets/img/my'+id+'.jpeg'
    fs.writeFile(fname, buf, function(err){
      console.log(error);
      result = "Error";
    });
    res.json(result);
  });

app.get('/', (req,res) => {
  res.sendFile(process.cwd()+"/sfmt/dist/dis-template/index.html")
});

// app.get('/', (req,res) => {
//   res.send('App Works !!!!');
// });

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});



